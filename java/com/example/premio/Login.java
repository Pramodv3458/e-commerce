package com.example.premio;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.loader.content.Loader;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Arrays;

import static android.provider.ContactsContract.Intents.Insert.EMAIL;

public class Login extends AppCompatActivity {
TextInputLayout email,password;
    int RC_SIGN_IN = 0;
    SignInButton signInButton;
    LoginButton login_button;
    TextView txtname,txtemail;
    private ProgressDialog loadingBar;
    GoogleSignInClient mGoogleSignInClient;
    CallbackManager callbackManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        email =findViewById(R.id.email);
        signInButton = findViewById(R.id.sign_in_button);
        loadingBar = new ProgressDialog(this);
        password =findViewById(R.id.password);
        login_button =findViewById(R.id.login_button);
        callbackManager = CallbackManager.Factory.create();
        login_button.setReadPermissions(Arrays.asList("email","public_profile"));
        login_button.registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {


                        startActivity(new Intent(Login.this,Home.class));
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                });
        ActionBar actionBar;
        actionBar = getSupportActionBar();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                signIn();
            }
        });
        ColorDrawable colorDrawable
                = new ColorDrawable(Color.parseColor("#FF5722"));

        // Set BackgroundDrawable
        actionBar.setBackgroundDrawable(colorDrawable);
    }
    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
        loadingBar.setTitle("Login");
        loadingBar.setMessage("Please wait,while allowing you to login into Account");
        loadingBar.show();
        loadingBar.setCanceledOnTouchOutside(true);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode,resultCode,data);
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
            loadingBar.dismiss();
        }
    }
    AccessTokenTracker tokenTracker = new AccessTokenTracker(){


        @Override
        protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
            if (currentAccessToken==null)
            {
                txtemail.setText("");
                txtname.setText("");
                Toast.makeText(getApplicationContext(),"User Logged Out",Toast.LENGTH_LONG).show();
            }
            else {
                loaduserproffile(currentAccessToken);
            }

        }
    };
    private  void loaduserproffile(AccessToken accessToken)
    {
        GraphRequest graphRequest = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {
                    String first_name =object.getString("first_name");
                    String last_name =object.getString("last_name");
                    String email =object.getString("email");
                    String id =object.getString("id");
                    String image_url ="https://graph.facebook.com/"+id+"/picture?type=normal";
                    txtname.setText(first_name+last_name);
                    txtemail.setText(email);
                    RequestOptions requestOptions = new RequestOptions();
                    requestOptions.dontAnimate();


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields","first_name,last_name,email,id");
        graphRequest.setParameters(parameters);
        graphRequest.executeAsync();
    }
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {

            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            // Signed in successfully, show authenticated UI

            startActivity(new Intent(Login.this, Home.class));
        } catch (ApiException e) {
            Log.w("Google Sign In Error", "signInResult:failed code=" + e.getStatusCode());
            Toast.makeText(Login.this, "Failed", Toast.LENGTH_LONG).show();
        }
    }
    @Override
    protected void onStart() {
        // Check for existing Google Sign In account, if the user is already signed in
        // the GoogleSignInAccount will be non-null.
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        if(account != null) {
            startActivity(new Intent(Login.this, Home.class));
        }
        super.onStart();
    }
    public void login(View view) {
        String e =email.getEditText().getText().toString();
        String p =password.getEditText().getText().toString();
        Intent n=getIntent();
        String e1=n.getStringExtra("email");
        String p1=n.getStringExtra("password");
        if (e.isEmpty())
        {
            email.setError("Enter the valid email");
            email.requestFocus();
        }
        else if (!e.contains("@"))
        {
            email.setError("Enter the valid email");
            email.requestFocus();
        }
        else if (p.isEmpty())
        {
            password.setError("Enter your Password");
            password.requestFocus();
        }
        else if (e1.equals(email.getEditText().getText().toString()) && p1.equals(password.getEditText().getText().toString()))
        {
            Intent intent =new Intent(Login.this,Home.class);
            intent.putExtra("name",e1);
            startActivity(intent);
        }
        else {
            Toast.makeText(getApplicationContext(),"Wrong Email or Password",Toast.LENGTH_LONG).show();
        }
    }

    public void register(View view) {
        startActivity(new Intent(this,Signup.class));
    }

}
